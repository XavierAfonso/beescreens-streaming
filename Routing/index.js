const express = require('express');
const fs = require('fs');
const app = express();
let path = require('path');
let assets = "assets/"
let http = require('http').Server(app);
let io = require('socket.io')(http);
app.io = io;

let cpt = 0;

// List Video streams
let videos = ["video1.mp4", "video2.mp4", "video3.mp4","screensaver.mp4"];
let values = new Array();
let freeVideos = [3,2,1,0];

app.use(express.static(__dirname + '/public'));

//Identify Displayers
io.on('connection', function(socket){

  console.log('a user connected : ' + socket.id);
  values[String(socket.id)] = freeVideos[freeVideos.length-1];
  freeVideos.pop();

  socket.emit('title',videos[values[String(socket.id)]]);

  /*console.log(values[String(socket.id)]);
  console.log(freeVideos.length);*/

  socket.on('disconnect', function(){
    console.log('user disconnected : ' + socket.id);
    freeVideos.push(values[String(socket.id)]);
    delete values[String(socket.id)];
  });
});

//Create Displayers
app.get('/displayers', function(req, res) {

  if(freeVideos.length>0){
    res.sendFile(__dirname + '/index.html');
  }

  else{
    res.send("<H1>Pas de vidéo disponible</H1>")
  }

});


app.get('/video/:id', function(req, res) {

  var id = req.params.id;

  if((id in values)){

    /*console.log(values[String(id)]);
    console.log(freeVideos.length);*/

    let currentPath =  assets + videos[values[String(id)]];
    fs.createReadStream(currentPath).pipe(res)
  }

});


http.listen(3000, function () {
  console.log('Listening on port 3000!')
});


