var express = require('express')
var app = express()
const fs = require('fs')
const path = require('path')
let sizetotal = 0;


//Stream a video. We can change the progress bar.
app.get('/video', function(req, res) {
  const path = 'assets/sample.mp4'
  const stat = fs.statSync(path)
  const fileSize = stat.size
  const range = req.headers.range

  //After the seconde request
  if (range) {

    console.log(range)

    //Determine the start and the end
    //When not available use the complete file size
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1] ? parseInt(parts[1], 10): fileSize-1
    const chunksize = (end-start)+1

    console.log("SIZE CHUNK :" + chunksize)

    const file = fs.createReadStream(path, {start, end}).on('data',(chunk)=>{ //highWaterMark: To change the chunksize, default max size : 665536 bytes
      sizetotal+=chunk.length
      console.log("I read" + sizetotal)
    })

    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
    }

    res.writeHead(206, head); //206 Partial Content
    file.pipe(res);

  } else { //The first time, because req.headers.range is null

    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    }

    res.writeHead(200, head)
    fs.createReadStream(path).pipe(res)
  }
});


//Stream video without the possiblity to change the progress bar
app.get('/continuousvideo', function(req, res) {

  const path = 'assets/sample.mp4'
  const head = {
    'Content-Type': 'video/mp4',
  }
  res.writeHead(200, head)
  fs.createReadStream(path).pipe(res) //{ start: 0, end: 5000000 } environ 6 //Stream a part 

});

//Observe the number of chunk read
app.get('/data', function(req, res) {

  const path = 'assets/sample.mp4'
  let read = fs.createReadStream(path)

  read.on('data',(chunk)=>{
    console.log("I read" + chunk.length)
  })

  res.send('Test')

});


app.listen(3000, function () {
  console.log('Listening on port 3000!')
});